@echo off
 :: ----------------------------------------------------------------
 title Virtual desktop app installer for %username%
 :: version 1.0
 :: date 03-03-2018
 :: author Boar Artist
 :: ----------------------------------------------------------------

 :: do the work

 set "_autostartDir=%appdata%\Microsoft\Windows\Start Menu\Programs\Startup"
 set _appName=my-virtual-desktop-enhancer
 set _appFullName=my-virtual-desktop-enhancer.exe
 set "_appNewDir=%appdata%\_Windows10VirtualDesktopEnhancer"
 set "_app=%_appNewDir%\%_appFullName%"

 :: copy exe file to %appdata%
 echo Installation...
 xcopy .\app\win10x64\* "%_appNewDir%" /S
 echo.

 :: do the link for startup
 :askLink
 SET /P continue="Do You want to ADD application on STARTUP for user %username% (y/n): "
 IF %continue% EQU y (
  goto :doLink
 ) ELSE (
  goto :ending
 )

:doLink
 set "_linkAbsoluteFile=%_autostartDir%\%_appName%.lnk"

 IF EXIST "%_linkAbsoluteFile%" (
    echo Application %_appFullName% already set on system startup.
	SET /P continue="Do You want to REMOVE application from STARTUP for user %username% (y/n): "
	IF %continue% EQU y (
		del "%_linkAbsoluteFile%" 
    )
	IF NOT EXIST "%_linkAbsoluteFile%" (
	  echo.
	  echo Link "%_linkAbsoluteFile%" was successfully DELETED.
	  echo Application %_appFullName% won't start on system startup for user %username%
	  echo.
	  goto :ending
	) ELSE (
		echo Script didn't remove applicaton from startup. Look at previliges for file: "%_linkAbsoluteFile%".
	)
 )

 echo Set WshShell = WScript.CreateObject("WScript.Shell") > CreateShortcut.vbs
 echo sLinkFile = "%_linkAbsoluteFile%" >> CreateShortcut.vbs
 echo Set oMyShortcut = WshShell.CreateShortcut(sLinkFile) >> CreateShortcut.vbs
 echo oMyShortcut.TargetPath = "%_app%" >> CreateShortcut.vbs
 echo oMyShortcut.IconLocation = "%_app%, 0" >> CreateShortcut.vbs
 echo OMyShortcut.WorkingDirectory = "%_appNewDir%" >> CreateShortcut.vbs
 echo oMyShortcut.Save >> CreateShortcut.vbs
 cscript CreateShortcut.vbs
 CreateShortcut.vbs
 del CreateShortcut.vbs

 IF EXIST "%_linkAbsoluteFile%" (
  echo.
  echo Link "%_linkAbsoluteFile%" was successfully created.
  echo Application %_appFullName% will start on system startup for user %username%
  echo.
  timeout 3 > NUL
  goto :ending
 ) ELSE (
 	 goto :askLink
 )

 :ending
  echo Script will exit in 3 seconds...
  timeout 3 > NUL
  exit