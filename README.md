# Custom Virtual Desktop upon Windows 10

If you want to prepare special key-bindings You need to look at [sources](https://github.com/sdias/win-10-virtual-desktop-enhancer). There are everything in subject also author talking about [AutoHotkey](https://autohotkey.com/download/) application.

For now if You already have Yours key-bind-app as _*.exe_ file You need to just run it.

## Tips
1. Because of bugs, it is better to turn **off Windows Game Bar**. It always displaying while using standalone _LAlt_ key shortcut to do something.
2. For me, last release (exe file) from already mentioned _sources_ has many bugs, but compiling _*.ahk_ file via compiler included in _AutoHotkey_ resolve bugs when run also as a common user.
3. Application may need to run _[Visual C++ Redistributable for Visual Studio 2015](https://www.microsoft.com/en-us/download/details.aspx?id=52685)_ but **for Windows 10 with 2017 version, is not needed**.
3. To run compiled application on startup, just run _Batch_ [script](./installScript.bat) included in this repo:

    ```cmd
    installScript.bat
    ```

_2018, Boar Artist_